"use strict";
// tslint:disable:object-literal-sort-keys
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    networks: {
        mainnet: {
            bip32: {
                private: 0x2BF4530,
                public: 0x2BF4968,
            },
            name: 'mainnet',
            nethash: '08e87c7b13b9d9505944b0cfc57644330098bdcf04af7fe7bf2864c2d0feeba5',
            token: 'BIN',
            symbol: '⟐',
            version: 0x19,
            explorer: 'https://explorer.bancoinplatform.com/',
            wif: 0x1F,
            p2pPort: 4001,
            apiPort: 4003,
            p2pVersion: '2.5.28',
            isV2: true,
            activePeer: {
                ip: '207.246.67.116',
                port: 4003,
            },
            peers: [
                '207.246.67.116:4003',
            ],
        },
        devnet: {
            bip32: {
                private: 0x2BF4530,
                public: 0x2BF4968,
            },
            name: 'mainnet',
            nethash: '08e87c7b13b9d9505944b0cfc57644330098bdcf04af7fe7bf2864c2d0feeba5',
            token: 'BIN',
            symbol: '⟐',
            version: 0x19,
            explorer: 'https://explorer.bancoinplatform.com/',
            wif: 0x1F,
            p2pPort: 4001,
            apiPort: 4003,
            p2pVersion: '2.5.28',
            isV2: true,
            activePeer: {
                ip: '207.246.67.116',
                port: 4003,
            },
            peers: [
                '207.246.67.116:4003',
            ],
        },
    },
    blockchain: {
        interval: 8,
        delegates: 51,
        date: new Date(Date.UTC(2020, 1, 7, 13, 1, 6, 776)),
    },
};
//# sourceMappingURL=index.js.map